var http = require('http');
var static = require('node-static');
var file = new static.Server('.');
var request = require('request');
var fs = require('fs');

http.createServer(function (req, res) {
    if (/\/src\/\d+/.test(req.url)) {
        req.url = 'http://localhost:8080/src/';
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(fs.readFileSync('./src/index.html'));
    } else {
        if (req.url.indexOf('/api') !== -1 ||
            req.url.indexOf('/md_') !== -1 ||
            req.url.indexOf('/sm_') !== -1) {
            request('http://ambresolaire.flashambush.ru' + req.url, function (error, response, body) {
                if (!error) {
                    if (response.statusCode === 302) {
                        debugger;
                    } else {
                        res.writeHead(response.statusCode, response.headers);
                        res.end(response.body);
                        console.log(req.url);
                    }
                } else {
                    res.writeHead(500, {});
                    res.end(error);
                    console.log(req.url, error.message, error.stack, error);
                }
            });
        } else {
            file.serve(req, res);
        }
    }
}).listen(8080);

console.log('Server running on port 8080');