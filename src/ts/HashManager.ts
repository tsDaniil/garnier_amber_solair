module app {
    'use strict';
    
    export class HashManager {
        
        public get(name: string): string|number {
            return this._parseHash()[name];
        }
        
        public set(name: string, value: string|number): void {
            let hashObject = this._parseHash();
            let newHash = '#';
            if (value === null && (name in hashObject)) {
                delete hashObject[name];   
            } else {
                hashObject[name] = value;
            }
            
            newHash += Object.keys(hashObject).map((name: string) => {
                return `${name}=${hashObject[name]}`;
            }).join('&');

            if (newHash === '#') {
                newHash = '';
            }

            let scroll = document.body.scrollTop;
            location.hash = newHash;
            document.body.scrollTop = scroll;
        }
        
        private _parseHash(): Object {
            let result = {};
            let hash = location.hash;
            
            if (hash[0] === '#') {
                hash = hash.substr(1);
            }
            
            hash.split('&').filter(Boolean).forEach((expration: string) => {
                if (expration.indexOf('=') === -1) {
                    result[expration] = true;
                } else {
                    let data = expration.split('=');
                    result[data[0]] = data[1];
                }
            });
            return result;
        }

        public parseSearch(): Object {
            let result = {};
            let hash = location.search;

            let url = location.href;
            if (url[url.length - 1] === '/') {
                url = url.slice(0, -1);
            }
            let path = url.split('/');
            let id = Number(path[path.length - 1]);
            if (id !== null && !isNaN(id)) {
                return {beach: id};
            }

            if (hash[0] === '?') {
                hash = hash.substr(1);
            }

            hash.split('&').filter(Boolean).forEach((expration: string) => {
                if (expration.indexOf('=') === -1) {
                    result[expration] = true;
                } else {
                    let data = expration.split('=');
                    result[data[0]] = data[1];
                }
            });
            return result;
        }
        
    }
    
}
