module app {
    'use strict';
    
    export function fetch(options: IFetchOptions): Promise<any> {
        return new Promise((resolve: any, reject: any) => {
            options.type = options.type || 'GET';
            (<any>options).success = resolve;
            (<any>options).error = reject;
            $.ajax(options)
        }).fail((e) => console.error(e));
    }
    
    export interface IFetchOptions {
        url: string;
        type?: string;
        data?: any;
    }
    
}