/// <reference path="../../typings/tsd.d.ts"/>
/// <reference path="jquery.jcarousel.min.ts"/>
/// <reference path="jcarousel.responsive.ts"/>
/// <reference path="Promise.ts"/>
/// <reference path="Fetch.ts"/>
/// <reference path="Map.ts"/>
/// <reference path="AddBeachManager.ts"/>
/// <reference path='HashManager.ts' />


let activeVideo;
declare let ymaps: any;
module app {

    let hasCollaje = false;

    let $caruselItem = $.get('./templates/carusel-item.hbs').then((template) => template);

    export let $hasPages = $.when(
        $.get('/api/gallery').then((data) => data)
    ).then((pages) => {
        return $caruselItem.then((template) => {
            let func = Handlebars.compile(template);
            let list = $('ul.gallery__slider.clearfix').empty();
            pages.forEach((page) => {
                list.append(func(page));
            });
        });
    });

    export function getCookie(name: string): string {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    export let hashManager = new HashManager();
    export let map;
    export let authVideoCounter = 0;

    export function showRating(element?: JQuery): void {

        element = element || $('body');

        let check = function () {
            setTimeout(() => {
                if (element.find(".raiting span").length) {
                    var tags = [];
                    element.find(".raiting span").each(function () {
                        if (jQuery(this).text().indexOf("рейтинг") != -1) {
                            tags.push(jQuery(this));
                        }
                    });

                    var replaceTag = function (tag) {
                        var max = 5;
                        var count = Math.floor(Number(tag.find('.mark').text()));
                        var html = "";
                        for (var i = 0; i < max; i++) {
                            if (i < count) {
                                html += "<span class='star active'></span>";
                            } else {
                                html += "<span class='star'></span>";
                            }
                        }
                        tag.html(html);

                        let id = tag.parent().attr('data-id');
                        let sended = false;

                        if (id) {
                            tag.parent().find('.send').click(() => {

                                openAuthModal().then(() => {
                                    sended = true;
                                    tag.parent().find('.send').hide();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/api/beach/rate',
                                        data: {
                                            id: id,
                                            mark: tag.attr('data-stars')
                                        }
                                    }).then((data) => {
                                        if (data.mark) {
                                            let stars = Math.floor(Number(data.mark));

                                            tag.children().each((index, elem) => {
                                                $(elem).toggleClass('active', index <= stars);
                                            });

                                            tag.attr('data-stars', stars);
                                            tag.parent().find('.mark-result').text(data.mark);
                                        }
                                    });
                                })
                            });
                        }

                        let width = tag.children().first().width();
                        let onClick = (e) => {

                            if (sended) {
                                return null;
                            }

                            let offset = tag.offset().left;
                            let delta = e.pageX - offset;
                            let stars = Math.floor(delta / width);

                            tag.children().each((index, elem) => {
                                $(elem).toggleClass('active', index <= stars);
                            });

                            stars++;
                            tag.attr('data-stars', stars);
                        };

                        if (tag.parent().hasClass('choose')) {
                            tag.hover(() => {
                                tag.on('click', onClick);
                            }, () => {
                                tag.off('click', onClick);
                            });
                        }
                    };

                    tags.forEach(function (tag) {
                        replaceTag(tag);
                    });
                } else {
                    setTimeout(check, 50);
                }
            }, 50);
        };

        check();
    }

    export function openWin(url: string): JQueryPromise<any> {
        let $def = $.Deferred();
        let width = 700;
        let height = 520;
        let left = innerWidth / 2 - width / 2;
        let top = innerHeight / 2 - height / 2;
        let win = window.open(url, 'auth', `left=${left},top=${top},width=${width},height=${height},menubar=no,toolbar=no,location=no,status=no`);

        let checkWindow = () => {
            setTimeout(() => {
                if (!win.closed) {
                    checkWindow();
                } else {
                    if ($def.state() === 'pending') {
                        $def.reject();
                    }
                }
            }, 500);
        };

        checkWindow();
        (<any>window).authDeffer = $def;
        return $def;
    }

    $(document).on('click', '.share-modal', function (e: JQueryEventObject) {

        openWin($(this).attr('href'));
        e.preventDefault();

    });

    export function openAuthModal(urls?: {name: string, method: () => JQueryPromise<any>}[]): JQueryPromise<any> {
        let $def = $.Deferred();
        let modal = (<any>$)('#authModal').modal();

        let stop = () => {
            modal.off();
            modal.find('.js-auth').off();
        };

        urls = urls || [
                {
                    name: 'fb',
                    method: authFb
                },
                {
                    name: 'vk',
                    method: authVk
                }
            ];

        modal.find('.js-auth').on('click', function (e: JQueryMouseEventObject) {
            if (urls) {
                urls.some(function (data) {
                    if ($(this).hasClass(data.name)) {
                        data.method().then($def.resolve, $def.reject);
                        return true;
                    }
                }, this);
            }
            stop();
            modal.modal('hide');
            e.preventDefault();
            e.stopPropagation();
        });

        modal.on('hide.bs.modal', () => {
            $def.reject();
            stop();
        });

        return $def.promise();
    }

    export function openAuthWarningModal(): any {
        (<any>$)('#authModalWarning').modal()
    }

    export function openAuthWarningModalVideo(): any {
        return (<any>$)('#authModalWarningVideo').modal()
    }

    export function authFb(): JQueryPromise<any> {
        return openWin('/api/facebook');
    }

    export function authVk(): JQueryPromise<any> {
        return openWin('/api/vk');
    }

    export function openCoolajeModal(): any {
        return (<any>$)('#createCoolajeModal').modal()
    }

    export function getVideo(): JQueryPromise<any> {

        let authVideoCounter = 0;
        let $defr = $.Deferred();

        let request = () => {
            if (authVideoCounter < 20) {
                $.get('/api/video').then((response) => {
                    if (response.status === 'success') {
                        $defr.resolve(response);
                        console.log('video url success');
                    } else if (response.status === 'too_few') {
                        $defr.reject();
                    } else {
                        setTimeout(request, 3000);
                    }
                }).fail($defr.reject);
            } else {
                $defr.reject();
                console.log('video url fail');
            }
            authVideoCounter++;
        };

        request();

        return $defr;
    }

    let end = false;

    $(function () {
        if ($('body').data('page') !== 'index') {
            return;
        }
        map = new Map();

        map.onReady().then(() => {
            $('body').animate({opacity: 1}, {duration: 600});
        });

        $('.add-beach').on('click', () => {

            let $def = $.Deferred();

            if (getCookie('auth.login')) {
                $def.resolve();
            } else {
                openAuthModal().then($def.resolve, $def.reject);
            }

            $def.then(() => {
                map.closeBalloon();
                let manager = new AddBeachManager();
                showRating(manager.getMainNode());

                map.showAddMode(manager.getDismisPromise());
            }, () => {
                openAuthWarningModal();
            });

        });

        map.onReady().then(() => {
            let timer = null;

            $('.js-choose-city').on('input', function () {

                if (timer) {
                    clearTimeout(timer);
                }

                timer = setTimeout(() => {
                    ymaps.geocode($(this).val(), {json: true}).then((data) => {
                        let geoCollection = data.GeoObjectCollection;
                        geoCollection.featureMember.filter((geoObject) => {
                            geoObject = geoObject.GeoObject;
                            return geoObject.description;
                        }).some((geoObject) => {
                            geoObject = geoObject.GeoObject;
                            map.map.setCenter(geoObject.Point.pos.split(' ').reverse());
                            map.map.setZoom(10);
                            return true;
                        });
                    });
                    timer = null;
                }, 600);

            });

        });

        function startLoader($def: JQueryPromise<{}>) {
            let node = $('<div class="block-all"><div class="preloader-my"></div></div>');
            $('body').append(node);
            $def.always(() => {
                node.remove();
            });
        }

        $('.js-video-block-play-btn').click(() => {
            if ($('.js-video source').attr('src') || end) {
                return;
            }
            let $def = $.Deferred();

            let buttonList = ['vk', 'fb'];

            let resolve = function () {

                let $dfr = $.Deferred();

                startLoader($dfr);

                setTimeout(() => {
                    getVideo().then((data) => {
                        $dfr.resolve();

                        $('.btn-cta_video-reload').attr('data-id', data.id);
                        if (activeVideo) {
                            activeVideo.reject();
                            activeVideo = null;
                        }
                        hasCollaje = true;
                        activeVideo = showVideo(data.id);
                        activeVideo.always(() => {
                            $('.js-video-block').addClass('_ended');
                            end = true;
                            $('.js-share').each(function () {
                                if ($(this).hasClass('vk')) {
                                    $(this).attr('href', `http://www.vk.com/share.php?url=http://идузагорать.рф/video/${data.id}`);
                                } else {
                                    $(this).attr('href', `http://www.facebook.com/sharer.php?s=100&p[url]=http://идузагорать.рф/video/${data.id}`);
                                }
                            });
                            activeVideo = null;
                        });

                    }, () => {
                        reject(openCoolajeModal);
                        $dfr.reject();
                    });
                }, 4000);
            };
            let reject = (modalCb) => {
                let modal = modalCb();

                modal.find('.js-auth').each(function () {
                    let el: HTMLElement = this;
                    let classList = Array.prototype.slice.call(el.classList);
                    if (buttonList.some((className: string) => classList.indexOf(className) === -1)) {
                        el.style.display = 'none';
                    }
                });

                let onClick = function (e: JQueryEventObject) {
                    e.preventDefault();
                    let url;
                    if ($(this).hasClass('vk')) {
                        url = '/api/vkvid';
                        buttonList = ['fb'];
                    } else {
                        url = '/api/fbvid';
                        buttonList = ['vk'];
                    }
                    openWin(url).then(resolve, reject);
                    modal.find('.js-auth').off();
                    modal.modal('hide');
                };

                modal.find('.js-auth').on('click', onClick);
            };

            openAuthModal([
                {
                    name: 'vk',
                    method: () => {
                        buttonList = ['fb'];
                        return openWin('/api/vkvid');
                    }
                },
                {
                    name: 'fb',
                    method: () => {
                        buttonList = ['vk'];
                        return openWin('/api/fbvid');
                    }
                }
            ]).then(resolve, reject.bind(null, openAuthWarningModalVideo));

        });

        $('.btn-cta_video-reload').on('click', function (e) {
            e.preventDefault();
            $('.js-video-block').removeClass('_ended');
            if (activeVideo) {
                activeVideo.reject();
                activeVideo = null;
            }
            activeVideo = showVideo($(this).attr('data-id'));
            activeVideo.then(() => {
                $('.js-video-block').addClass('_ended');
            });
        });
    });

    $(document).on('click', '.js-carusel-item', function () {
        if (activeVideo) {
            activeVideo.reject();
            activeVideo = null;
        }
        activeVideo = showVideo($(this).attr('data-id')).done(() => {
            if (end) {
                $('.js-video-block').addClass('_ended');
            }
        });
        $('body').animate({
            scrollTop: $('.js-video-block').offset().top
        });
    });

    let hasVideo = false;
    let lastVideo: JQueryDeferred<any>;

    function showVideo(id: number|string): JQueryDeferred<{}> {
        hasVideo = true;

        if (lastVideo && lastVideo.state() === 'pending') {
            lastVideo.reject();
        }

        let $def = $.Deferred();
        lastVideo = $def;

        $('.js-video-block').removeClass('_ended');
        $('.js-video-block').prepend('<video class="js-video video-block__video-elem" controls autoplay></video>');
        $('.js-video').addClass('_has-src');
        $('.js-video').empty();
        $('.js-video').append(`<source src="http://asvid.flashambush.ru/${id}.mp4" type="video/mp4">`);

        (<HTMLVideoElement>$('.js-video')[0]).addEventListener('ended', $def.resolve);
        $def.always(() => {
            $('.js-video').remove();
            lastVideo = null;
            hasVideo = false;
        });

        return $def;
    }

    $.get('./templates/weather.tpl.hbs')
        .then((template) => {
            $.get('/api/weather/list').then((data) => {
                data.forEach((item) => {
                    var temp: any = Math.floor(item.temp);
                    if (temp > 0) temp = "+" + temp;
                    item.temp = temp;
                    $('.js-weather-wrapper').append(Handlebars.compile(template)(item))
                })
            });
        });

    $(document).on('click', '.top-image', function () {
        let url = $(this).attr('data-url');
        if (url) {
            (<any>$)('#imageModal').modal();
            $('#imageModal').find('img').attr('src', url);
            $('#imageModal').find('.modal-title').text($(this).attr('data-title'));
        }
    });

    //-- custom script for header menu --

    $(document).ready(function () {
        function initHeaderMobHandlers() {
            var $mobNavBtn = $('.mob-nav-btn');
            var $mobMenu = $('.mobile-main-nav');
            var $mobNavContent = $mobMenu.find('.nav-content-wrap');
            $mobNavBtn.click(function () {
                //$mobMenu.addClass('_opened');
                $mobMenu.fadeIn(150);
                $mobNavContent.delay(150).animate({'width': '75vw'}, 200);
            });
            $mobMenu.click(function (e) {
                //$mobMenu.removeClass('_opened');
                if (!$(e.target).is('.nav-content-wrap')) {
                    $mobMenu.delay(200).fadeOut(150);
                    $mobNavContent.animate({'width': '0'}, 200);
                }
            });
        }

        initHeaderMobHandlers();
    });

    interface IShareOptions {
        url: string;
        type?: string;
        title: string;
        description: string;
        image: string;
    }

    let setNewShare = function (id?) {

        let vk = $('.fake.vk');
        let fb = $('.fake.fb');

        [fb, vk].forEach((tag) => {
            let url = tag.attr('data-url');
            if (id) {
                // if (typeof id === 'string') {
                url = url + encodeURI(tag.attr('data-param') + `/${id}`);
                // } else {
                //     url = url + tag.attr('data-param') + `/${id}`;
                // }
            } else {
                url += tag.attr('data-param');
            }
            tag.attr('href', url);
        });
    };

    export function openBalloon(id): void {
        setNewShare(id);
    }

    export function closeBalloon(): void {
        setNewShare($('.js-choose-city').val().trim());
    }

    $(function () {
        $('.js-choose-city').on('change', closeBalloon);
    });
}
