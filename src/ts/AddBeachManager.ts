module app {
    'use strict';

    let template;
    let feedback;

    $.get('./templates/add-beach.tpl.hbs').then((data) => {
        template = data;
    });

    $.get('./templates/add-beach-feedback.tpl.hbs').then((data) => {
        feedback = data;
    });

    export class AddBeachManager {

        private node: JQuery;
        private $def: JQueryDeferred<any> = $.Deferred();
        private lat: number;
        private lon: number;
        private $title: JQuery;
        private $address: JQuery;
        private $file: JQuery;

        constructor() {
            this.init();
        }

        public getDismisPromise(): JQueryPromise<{}> {
            return this.$def.promise();
        }

        public getMainNode(): JQuery {
            return this.node;
        }

        private init(): void {
            this.addTemplate();
            this.setHandlers();
            this.createPoint(null);
            this.initFirstAddress();
        }

        private initFirstAddress(): void {
            map.onReady().then(() => {
                let latLon = map.map.getCenter();
                this.lat = latLon[0];
                this.lon = latLon[1];
                map.getAddressByPoint(this.lat, this.lon).then((text: string) => {
                    this.$address.val(text);
                });
            });
        }

        private createPoint(latLon: Array<number>): void {
            map.createItem(latLon, (lat: number, lon: number) => {
                this.lat = lat;
                this.lon = lon;
                ymaps.geocode([lat, lon]).then((res) => {
                    let firstGeoObject = res.geoObjects.get(0);
                    this.$address.val(firstGeoObject.properties.get('text'));
                });
            });
        }

        private setHandlers(): void {

            let self = this;

            function split(val) {
                return val.split(/,\s*/);
            }

            function extractLast(term) {
                return split(term).pop();
            }

            this.$file.on('change', () => {
                let file: File = this.getFile();
                if (file) {
                    let exits = ['jpeg', 'jpg', 'png'];
                    if (!exits.some((exist: string) => file.type.indexOf(exist) !== -1)) {
                        this.$file.val('');
                    }
                }
            });

            this.node.find('.close').on('click', () => {
                this.node.remove();
                this.$def.reject();
            });

            (<any>this.$address)
                .on({
                    "keydown": function (event) {
                        if (event.keyCode === (<any>$).ui.keyCode.TAB &&
                            (<any>$)(this).autocomplete("instance").menu.active) {
                            event.preventDefault();
                        }
                    },
                    'blur': function () {
                        let val = self.$address.val();
                        if (val) {
                            ymaps.geocode(val, {json: true}).then((data) => {
                                let geoCollection = data.GeoObjectCollection;
                                if (geoCollection.featureMember.length) {
                                    let geoObject = geoCollection.featureMember[0].GeoObject;
                                    let latLon = geoObject.Point.pos.split(' ').reverse();

                                    self.$address.val(geoObject.name);
                                    self.lat = latLon[0];
                                    self.lon = latLon[1];
                                    map.map.setCenter(latLon);
                                    self.createPoint(latLon);
                                } else {
                                    self.initFirstAddress();
                                }
                            });
                        }
                    }
                })
                .autocomplete({
                    delay: 600,
                    autoFocus: true,
                    appendTo: ".add-beach-form",
                    source: function (request, response) {
                        let text = request.term;
                        if (text.indexOf('Россия') === -1) {
                            text = `Россия ${text}`;
                        }
                        ymaps.geocode(text, {json: true}).then((data) => {
                            let geoCollection = data.GeoObjectCollection;
                            response(geoCollection.featureMember.filter((geoObject) => {
                                geoObject = geoObject.GeoObject;
                                return geoObject.description;
                            }).map((geoObject) => {
                                geoObject = geoObject.GeoObject;
                                return {
                                    id: `${geoObject.description}, ${geoObject.name}`,
                                    label: `${geoObject.description}, ${geoObject.name}`,
                                    value: `${geoObject.description}, ${geoObject.name}`,
                                    point: geoObject.Point.pos.split(' ').reverse()
                                }
                            }));
                        });
                    },
                    search: function () {
                        // custom minLength
                        var term = extractLast(this.value);
                        if (term.length < 3) {
                            return false;
                        }
                    },
                    focus: function () {
                        // prevent value inserted on focus
                        return false;
                    },
                    select: function (event, ui) {
                        var terms = split(this.value);
                        this.value = ui.item.value;
                        self.lat = ui.item.point[0];
                        self.lon = ui.item.point[1];
                        self.createPoint([ui.item.point[0], ui.item.point[1]]);
                        return false;
                    }
                });

            this.node.find('.save').on('click', () => {

                if (!this.validate()) {
                    return null;
                }

                let formData = new FormData();
                [
                    'title',
                    'address'
                ].forEach((name) => formData.append(name, this.node.find(`.${name}`).val()));

                // mark - оценка (1-5)
                // price - true, если платный пляж
                [
                    'chairs',
                    'cafe',
                    'parking',
                    'swimming',
                    'aquaFun',
                    'kidsPlace',
                    'barbecue'
                ].forEach((name) => {
                    let checked = this.node.find(`.${name}`).is(':checked');
                    formData.append(name, checked);
                });

                let file: File = this.getFile();

                if (file) {
                    formData.append('image', file, file.name);
                }

                let stars = Number(this.node.find('[data-stars]')) || 3;

                formData.append('mark', stars);
                formData.append('lat', Number(this.lat));
                formData.append('lon', Number(this.lon));

                let price = this.node.find('.price').filter(':checked').val() == 'true';
                formData.append('price', price);

                $.ajax({
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    url: '/api/beach/manage',
                    processData: false,
                    contentType: false,
                }).then((...args) => {
                    this.node.remove();
                    app.map.refetch();
                    this.$def.resolve();
                    this.showFeedback('Спасибо, пляж добавлен!<br/>Он появится на карте, как только пройдёт модерацию');
                }, () => {
                    this.$def.reject();
                    this.showFeedback('Не ужалось добавить пляж!')
                });

            });

        }

        private showFeedback(message: string): void {
            let $feedback = $('<div>', {
                className: 'feedback',
                html: Handlebars.compile(feedback)({text: message})
            });
            $feedback.find('button').click(() => $feedback.remove());
            $('#map').append($feedback);
        }

        private validate(): boolean {

            let valid: boolean = true;

            let title = this.$title.val().trim();

            if (!title) {
                valid = false;
            }
            this.$title.parent().toggleClass('has-error', !title);

            let address = this.$address.val();
            if (!address) {
                valid = false;
            }
            this.$address.parent().toggleClass('has-error', !address);

            let file = this.getFile();
            if (!file) {
                valid = false;
            }
            this.$file.parent().toggleClass('has-error', !file);

            return valid;
        }

        private getFile(): File {
            return (<HTMLInputElement>this.node.find('.file').get(0)).files[0];
        }

        private addTemplate(): void {
            $('.js-add-wrapper').append(Handlebars.compile(template)({}));
            this.node = $('.add-beach-form');

            this.$title = this.node.find('.title');
            this.$address = this.node.find('.address');
            this.$file = this.node.find('.file');
        }

    }

}