module app {
    'use strict';

    export class Promise<T> {

        private successHandelrs: Array<any> = [];
        private errorHanlers: Array<any> = [];
        private data: any;
        private state: boolean;

        constructor(cb: Promise.ICallback<T>) {

            let resolve: any = (data: T) => {
                this.setState(data, true);
            };

            let reject = (data: any) => {
                this.setState(data, false);
            };

            try {
                cb(resolve, reject);
            } catch (e) {
                reject(e);
            }
        }

        public then(success: Promise.ICallback<T>, error?): Promise<any> {
            return new Promise((resolve: any, reject) => {
                this.successHandelrs.push((data: T) => {
                    try {
                        let result = success(data);
                        if (result instanceof Promise) {
                            result.then(resolve, reject);
                        } else {
                            resolve(data);
                        }
                    } catch (e) {
                        console.error(e);
                        reject(e);
                    }
                });
                if (error) {
                    this.errorHanlers.push(error);
                }
                this.setState(this.data, this.state);
            });
        }

        public done(callback: Promise.ICallback<T>): Promise<T> {
            this.successHandelrs.push(callback);
            return this;
        }

        public fail(callback: Function): Promise<T> {
            this.errorHanlers.push(callback);
            return this;
        }

        private setState(data: any, state: boolean): void {
            if (state != null) {
                this.state = state;
                let callbacks;
                if (state) {
                    callbacks = this.successHandelrs.slice();
                } else {
                    callbacks = this.errorHanlers.slice();
                }
                this.errorHanlers = [];
                this.successHandelrs = [];
                callbacks.forEach((cb) => {
                    cb(data);
                });
            }
        }

        public static all<T>(promises: Array<Promise<T>>): Promise<Array<T>> {
            return new Promise((resolve: any, reject) => {
                let allData = [];
                let resolveCount = 0;
                promises.forEach((promise, index) => {
                    promise.then((data) => {
                        allData[index] = data;
                        resolveCount++;
                        if (resolveCount === promises.length) {
                            resolve(allData);
                        }
                    }, (data) => {
                        allData[index] = data;
                        reject(data);
                    });
                });
            });
        }

    }

    export module Promise {

        export interface ICallback<T> {
            (resolve: T, reject?: any): any
        }

    }

}
