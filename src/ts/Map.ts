module app {
    'use strict';

    let template;

    $.get('./templates/mapBalloon.tpl.hbs').then((data) => {
        template = data;
    });

    export class Map {

        private map: any;
        private items: Object = {};
        private forAdd: any;
        private timer = null;
        private balloon: any;
        private ready: Promise<{}>;

        constructor() {
            this._init();
        }

        public getAddressByPoint(lat?: number, lon?: number): Promise<string> {
            return this.onReady().then(() => {
                return new Promise((resolve: Function) => {
                    let latLon = lat && lon ? [lat, lon] : this.map.getCenter();
                    ymaps.geocode(latLon).then((res) => {
                        let firstGeoObject = res.geoObjects.get(0);
                        resolve(firstGeoObject.properties.get('text'));
                    });
                });
            });
        }

        public onReady(): Promise<{}> {
            return this.ready;
        }

        public centerOn(point: Array<number>, zoom: number): void {
            this.map.setCenter(point);
            this.map.setZoom(zoom);
        }

        public closeBalloon(): void {
            if (this.balloon && this.balloon.isOpen()) {
                this.balloon.close();
                closeBalloon();
            }
        }

        public refetch(): void {
            this._getMapData().then((data) => this._addData(data));
        }

        public showAddMode($promise: JQueryPromise<any>): void {
            this.ready.then(() => {
                // this._hideAllItems();
                $promise.always(() => {
                    // this._showAllItems();
                    if (this.forAdd) {
                        this.map.geoObjects.remove(this.forAdd);
                        this.forAdd = null;
                    }
                });
            });
        }

        public getVisibleBounce(): {lat1: number; lon1: number; lat2: number; lon2: number} {
            let bounce: Array<Array<number>> = this.map.getBounds();
            return {
                lat1: bounce[0][0],
                lon1: bounce[0][1],
                lat2: bounce[1][0],
                lon2: bounce[1][1]
            };
        }

        public createItem(latLon, onDrag): any {
            this.ready.then(() => {

                latLon = latLon || this.map.getCenter();

                if (this.forAdd) {
                    this.map.geoObjects.remove(this.forAdd);
                }
                let myGeoObject = new ymaps.GeoObject({
                    geometry: {
                        type: "Point",
                        coordinates: latLon
                    }
                }, {
                    draggable: true
                });

                myGeoObject.events.add('dragend', () => {
                    onDrag.apply(null, myGeoObject.geometry.getCoordinates());
                });
                this.map.setZoom(15);
                this.map.setCenter(latLon);
                this.forAdd = myGeoObject;
                this.map.geoObjects.add(myGeoObject);
            });
        }

        private _hideAllItems(): void {
            Object.keys(this.items).forEach((id: string) => {
                this.items[id].geoObject.options.set('visible', false);
            });
        }

        private _showAllItems(): void {
            Object.keys(this.items).forEach((id: string) => {
                this.items[id].geoObject.options.set('visible', true);
            });
        }

        private _init(): void {
            this.ready = this._createMap()
                .then(() => this.customizeControls())
                .then(() => this.setHandlers())
                .then(() => this._getMapData())
                .then((data) => this._addData(data))
                .then(() => {
                    let id = Number(hashManager.parseSearch()['beach'] || hashManager.get('beach')) || null;
                    if (id) {
                        this._openBalloonById(id);
                    }
                });
        }

        private setHandlers(): void {
            this.map.events.add('actionend', () => {
                if (this.timer) {
                    clearTimeout(this.timer);
                }
                this.timer = setTimeout(() => {
                    this.timer = null;
                    this._getMapData()
                        .then((data) => this._addData(data))
                }, 100);
            });
            this.map.events.add('click', () => {
                this.closeBalloon();
                hashManager.set('beach', null);
            });
        }

        private customizeControls(): void {
            let MyLayoutClass = ymaps.templateLayoutFactory.createClass('<div>{{content}}</div>');
            let zoomOut = new ymaps.control.Button({
                options: {
                    layout: ymaps.templateLayoutFactory.createClass(
                        `<div class="zoom zoom-out"></div>`
                    ),
                    maxWidth: 67
                }
            });
            let zoomIn = new ymaps.control.Button({
                options: {
                    layout: ymaps.templateLayoutFactory.createClass(
                        '<div class="zoom zoom-in"></div>'
                    ),
                    maxWidth: 67
                }
            });
            zoomOut.events.add('click', () => {
                let zoom = this.map.getZoom();
                this.map.setZoom(zoom - 1);
            });
            zoomIn.events.add('click', () => {
                let zoom = this.map.getZoom();
                this.map.setZoom(zoom + 1);
            });
            this.map.controls.add(zoomIn, {
                float: 'none',
                position: {
                    left: 10,
                    top: $('#map').height() / 2 - 77
                }
            });
            this.map.controls.add(zoomOut, {
                float: 'none',
                position: {
                    left: 10,
                    top: $('#map').height() / 2 + 10
                }
            });
        }

        private _addData(data): void {

            (data || []).forEach((item) => {

                if (this.items[item.id]) {
                    return null;
                }

                let myGeoObject = new ymaps.GeoObject({
                    geometry: {
                        type: "Point",
                        coordinates: [item.lat, item.lon]
                    }
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: './img/zont.png'
                });

                this.items[item.id] = item;

                myGeoObject.events.add('click', () => {
                    hashManager.set('beach', item.id);
                    this._openBalloonById(item.id);
                });

                this.items[item.id].geoObject = myGeoObject;

                this.map.geoObjects.add(myGeoObject);
            });
        }

        private _openBalloonById(id: number): void {
            this._getPointData(id).then((data) => {

                if (!this.items[id]) {
                    this._addData([data]);
                }
                let item = this.items[id];
                openBalloon(id);

                this.closeBalloon();
                this.balloon = new ymaps.Balloon(this.map, {
                    layout: ymaps.templateLayoutFactory.createClass(Handlebars.compile(template)(data))
                }, {zIndex: 10000});
                this.map.setCenter([item.lat, item.lon]);
                this.balloon.options.setParent(this.map.options);
                this.balloon.open([item.lat, item.lon], {}, {
                    offset: [19, -27]
                });

                setTimeout(() => {
                    $(`.balloon.for-${id}`).parent().parent().parent().css('zIndex', 10000);
                }, 300);
                showRating();
            });
        }

        private _getMapData(): Promise<{}> {
            return fetch({
                url: '/api/beach/list',
                data: this.getVisibleBounce()
            });
        }

        private _getPointData(id: number): Promise<any> {
            return fetch({
                url: '/api/beach/get',
                data: {id: id}
            });
        }

        private _createMap(): Promise<{}> {
            return new Promise((resolve: Function, reject) => {
                ymaps.ready(() => {
                    // Пример определения региона пользователя
                    // и создания карты, показывающей этот регион
                    ymaps.geolocation.get().then((res) => {
                        // Предполагается, что на странице подключен jQuery
                        let $container = $('map'),
                            bounds = res.geoObjects.get(0).properties.get('boundedBy'),
                            mapState = ymaps.util.bounds.getCenterAndZoom(
                                bounds,
                                [$container.width(), $container.height()]
                            );
                        mapState.zoom = 10;
                        mapState.controls = [];
                        try {
                            this.map = new ymaps.Map('map', mapState, {
                                maxZoom: 18,
                                minZoom: 3
                            });
                        } catch (e) {
                            reject(e);
                        }
                        resolve();
                    }, (e) => {
                        this.map = new ymaps.Map('map', {
                            center: [55.76, 37.64],
                            zoom: 10,
                            controls: []
                        }, {
                            maxZoom: 18,
                            minZoom: 3
                        });
                        resolve();
                    }).fail(Map.onError);
                });
            });
        }

        private static onError(e): void {
            console.error(e);
        }
    }

    export interface Map {


    }

}
